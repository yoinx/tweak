package com.yoinx.tweak.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;


import com.yoinx.tweak.R;
import com.yoinx.tweak.Tweak;
import com.yoinx.tweak.objects.DividerObject;
import com.yoinx.tweak.objects.SeekbarObject;
import com.yoinx.tweak.objects.SpinnerObject;
import com.yoinx.tweak.objects.SwitchObject;
import com.yoinx.tweak.objects.Tweaks;
import com.yoinx.tweak.viewHolders.ViewHolderDividerCard;
import com.yoinx.tweak.viewHolders.ViewHolderSeekbarCard;
import com.yoinx.tweak.viewHolders.ViewHolderSpinnerCard;
import com.yoinx.tweak.viewHolders.ViewHolderSwitchCard;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int SWITCH = 1, SEEKBAR = 2, SPINNER = 3, DIVIDER = 4;
    private ArrayList<Tweaks> viewList;
    private Context mContext;
    private Tweak.RecyclerAdapterListener itemListener;

    public RecyclerAdapter(Context context, Tweak.RecyclerAdapterListener listener, ArrayList<Tweaks> viewList) {
        mContext = context;
        this.viewList = viewList;
        itemListener = listener;
    }

    @Override
    public int getItemCount() {
        return this.viewList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case SWITCH:
                View switchView = inflater.inflate(R.layout.card_switch, viewGroup, false);
                viewHolder = new ViewHolderSwitchCard(switchView, itemListener);
                break;
            case SEEKBAR:
                View seekbarView = inflater.inflate(R.layout.card_seekbar, viewGroup, false);
                viewHolder = new ViewHolderSeekbarCard(seekbarView, itemListener);
                break;
            case SPINNER:
                View spinnerView = inflater.inflate(R.layout.card_spinner, viewGroup, false);
                viewHolder = new ViewHolderSpinnerCard(spinnerView, itemListener);
                break;
            case DIVIDER:
                View dividerView = inflater.inflate(R.layout.card_divider, viewGroup, false);
                viewHolder = new ViewHolderDividerCard(dividerView);
                break;
            default:
                viewHolder = null;
                break;
        }
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        if (viewList.get(position) instanceof SwitchObject) {
            return SWITCH;
        } else if (viewList.get(position) instanceof SeekbarObject) {
            return SEEKBAR;
        } else if (viewList.get(position) instanceof SpinnerObject) {
            return SPINNER;
        } else if (viewList.get(position) instanceof DividerObject) {
            return DIVIDER;
        } else {
            return -1;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case SWITCH:
                ViewHolderSwitchCard switchVH = (ViewHolderSwitchCard) viewHolder;
                switchVH.setSwitchObject((SwitchObject) viewList.get(position));
                break;
            case SEEKBAR:
                ViewHolderSeekbarCard seekbarVH = (ViewHolderSeekbarCard) viewHolder;
                seekbarVH.setSeekbarObject((SeekbarObject) viewList.get(position), mContext);
                break;
            case SPINNER:
                ViewHolderSpinnerCard spinnerVH = (ViewHolderSpinnerCard) viewHolder;
                spinnerVH.setSpinnerObject((SpinnerObject) viewList.get(position), mContext);
                break;
            case DIVIDER:
                ViewHolderDividerCard dividerVH = (ViewHolderDividerCard) viewHolder;
                configureViewHolderDivider(dividerVH, position);
                break;
            default:
                break;
        }
    }

    private void configureViewHolderDivider(ViewHolderDividerCard dividerhCardViewHolder, int position) {
        DividerObject dividerObject = (DividerObject) viewList.get(position);
        if (dividerObject != null) {
            if (dividerObject.getCardText() != null) {
                dividerhCardViewHolder.getMainText().setText(dividerObject.getCardText());
            }
        }
    }
}