package com.yoinx.tweak.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.yoinx.tweak.R;
import com.yoinx.tweak.Tweak;
import com.yoinx.tweak.utils.JsonParser;
import com.yoinx.tweak.utils.TweakValue;
import com.yoinx.tweak.adapters.RecyclerAdapter;
import com.yoinx.tweak.objects.SeekbarObject;
import com.yoinx.tweak.objects.SpinnerObject;
import com.yoinx.tweak.objects.SwitchObject;
import com.yoinx.tweak.objects.Tweaks;

import java.util.ArrayList;

public class Tweakables extends Fragment implements Tweak.RecyclerAdapterListener {
    ArrayList<Tweaks> tweaksArray;
    String category;
    JsonParser jsonParser;
    TweakValue tweakValue;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            category = getArguments().getString("category");
        }
        jsonParser = new JsonParser(getContext());

        if (tweaksArray == null) {
            tweaksArray = new ArrayList<>();
        }
        tweaksArray = jsonParser.getTweaks(category);
        tweakValue = new TweakValue(getContext());

        View view = inflater.inflate(R.layout.fragment_recycler, container, false);
        RecyclerView fragViewList = view.findViewById(R.id.recyclerview);
        RecyclerAdapter adapter = new RecyclerAdapter(getContext(), this, tweaksArray);
        fragViewList.setAdapter(adapter);
        fragViewList.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void switchToggled(SwitchObject sw, boolean state) {
        tweakValue.saveValue(sw.getPath(), state);
    }

    @Override
    public void seekbarChanged(SeekbarObject sw, int position, int value) {
        tweakValue.saveValue(sw.getPath(), value);
    }
    @Override
    public void spinnerItemSelected(SpinnerObject spin, int position, String value){
        tweakValue.saveValue(spin.getPath(), value);
        Toast.makeText(getContext(), "Spinner selected " + value + " at position " + position, Toast.LENGTH_SHORT).show();
    }

}