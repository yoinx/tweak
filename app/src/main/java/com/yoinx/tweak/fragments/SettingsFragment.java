package com.yoinx.tweak.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.yoinx.tweak.R;
import com.yoinx.tweak.Tweak;
import com.yoinx.tweak.utils.Settings;
import com.yoinx.tweak.adapters.RecyclerAdapter;
import com.yoinx.tweak.objects.SeekbarObject;
import com.yoinx.tweak.objects.SpinnerObject;
import com.yoinx.tweak.objects.SwitchObject;
import com.yoinx.tweak.objects.Tweaks;

import java.util.ArrayList;

public class SettingsFragment extends Fragment implements Tweak.RecyclerAdapterListener {

    ArrayList<Tweaks> items = new ArrayList<>();
    Settings settings;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        settings = new Settings(mContext);
        View view = inflater.inflate(R.layout.fragment_recycler, container, false);
        RecyclerView fragViewList = view.findViewById(R.id.recyclerview);
        RecyclerAdapter adapter = new RecyclerAdapter(getContext(), this, getItems());
        fragViewList.setAdapter(adapter);
        fragViewList.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    private ArrayList getItems() {

        items.add(
                new SwitchObject()
                .setPath(settings.VERBOSELOGGING)
                .setCardText(getString(R.string.settings_verbose_logging))
                .setSupplementalText(getString(R.string.settings_verbose_logging_detail))
                .setContext(getContext())
        );

        ArrayList<String> options = new ArrayList<>();

        for (int i = 1; i <= 60; i++) {
            options.add(String.valueOf(i));
        }

        items.add(
                new SpinnerObject()
                .setOptions(options)
                .setCardText(getString(R.string.settings_set_on_boot_delay))
                .setSupplementalText(getString(R.string.settings_set_on_boot_delay_detail))
                .setPath(settings.APPLYONBOOTDELAY)
                .setCurrentValue(Integer.toString(settings.getInt(settings.APPLYONBOOTDELAY, settings.getDefaultInt(settings.APPLYONBOOTDELAY))))
        );

        return items;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void switchToggled(SwitchObject sw,  boolean state) {
        settings.saveSetting(sw.getPath(), state);
    }

    @Override
    public void seekbarChanged(SeekbarObject sw, int position, int value) {
        Toast.makeText(getContext(), sw.getCardText() + " with path " + sw.getPath() + " set to " + value, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void spinnerItemSelected(SpinnerObject spin, int position, String value){
        if (spin.getPath().equals(settings.APPLYONBOOTDELAY)) {
            settings.saveSetting(spin.getPath(), Integer.valueOf(value));
        }
    }


}
