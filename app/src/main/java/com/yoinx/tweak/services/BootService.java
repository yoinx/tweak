package com.yoinx.tweak.services;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.yoinx.tweak.R;
import com.yoinx.tweak.utils.SU;
import com.yoinx.tweak.utils.Settings;
import com.yoinx.tweak.utils.TweakValue;
import com.yoinx.tweak.utils.Notification;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class BootService extends JobIntentService {

    public static final int JOB_ID = 0x01;
    private SU su = new SU();

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, BootService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        applySettings(getApplicationContext());
    }

    public void applySettings (final Context context) {
        TweakValue tweakValue = new TweakValue(context);
        Settings settings = new Settings(context);
        Settings stabilityCheck = new Settings(context, settings.STABILITY);
        String currentUname = su.getSyncCommandOutput("uname -r");
        String uname = settings.getString(stabilityCheck.STABILITY_KERNEL,"");
        int settingsDelay = settings.getInt(settings.APPLYONBOOTDELAY, settings.getDefaultInt(settings.APPLYONBOOTDELAY));

        final HashMap<String, String> values = tweakValue.getAll();
        Log.d("Tweak", "current uname: '" + currentUname + "' stored uname: '" + uname + "'");

        if (values.size() > 0) {
            final Notification notification = new Notification(context);
            if (currentUname.equals(uname)) {
                notification.sendNotification(
                        String.format(
                                context.getString(R.string.bootservice_applying_values_after_delay),
                                settingsDelay),
                        null, 1, settingsDelay);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Log.d(context.getString(R.string.app_name), context.getString(R.string.bootservice_applying_values));
                        for (Map.Entry<String, String> entry : values.entrySet()) {
                            Log.d(context.getString(R.string.app_name), String.format(context.getString(R.string.applying_value_to_path), entry.getValue(), entry.getKey()));
                            su.writeFile(entry.getKey(), entry.getValue());
                        }
                        notification.sendNotification(
                                context.getString(R.string.app_name),
                                context.getString(R.string.bootservice_complete), 1, 0);
                        this.cancel();
                    }
                }, settingsDelay * 1000);
            } else {
                notification.sendNotification("Kernel Mismatch",
                        null, 1, 0);
                Log.d("Tweak", "Kernel Did not match.");
            }
        }
    }
}