package com.yoinx.tweak.viewHolders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.yoinx.tweak.R;
import com.yoinx.tweak.Tweak;
import com.yoinx.tweak.objects.SeekbarObject;

public class ViewHolderSeekbarCard extends RecyclerView.ViewHolder implements SeekBar.OnSeekBarChangeListener {
    private TextView mainText;
    private TextView currentValue;
    private SeekBar mSeekbar;
    private Tweak.RecyclerAdapterListener mListener;
    private int mProgress = 0;
    private SeekbarObject seekbarObject;

    public ViewHolderSeekbarCard(View v, Tweak.RecyclerAdapterListener listener) {
        super(v);
        mainText = v.findViewById(R.id.card_seekbar_text_main);
        currentValue = v.findViewById(R.id.card_seekbar_current_value);
        mSeekbar = v.findViewById(R.id.card_seekbar_seekBar_widget);
        mListener = listener;
    }

    public void setSeekbarObject (SeekbarObject seek, Context context) {
        seekbarObject = seek;
        mainText.setText(seekbarObject.getCardText());
        if (sanityCheck()) {
            mSeekbar.setMax((seekbarObject.getMax() - seekbarObject.getMin()) / seekbarObject.getStep());
            mSeekbar.setProgress(((seekbarObject.getValue() - seekbarObject.getMin()) / seekbarObject.getStep()));
            mSeekbar.setOnSeekBarChangeListener(this);
            currentValue.setText(String.valueOf(seekbarObject.getValue()));
        } else {
            mSeekbar.setEnabled(false);
            currentValue.setText(context.getString(R.string.switchcard_failed_sanity_check));
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mProgress = ((progress * seekbarObject.getStep()) + seekbarObject.getMin());
        currentValue.setText(String.valueOf(mProgress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // Disregard as only the final position matters for this usage
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mListener.seekbarChanged(seekbarObject, getAdapterPosition(), mProgress);
    }

    private boolean sanityCheck() {
        return ( (seekbarObject.getMax() - seekbarObject.getMin()) % seekbarObject.getStep() == 0 );
    }

}
