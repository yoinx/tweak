package com.yoinx.tweak.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.yoinx.tweak.R;

public class ViewHolderDividerCard extends RecyclerView.ViewHolder {
    private TextView mainText;

    public ViewHolderDividerCard(View v) {
        super(v);
        mainText = (TextView) v.findViewById(R.id.card_divider_text_main);
    }

    public TextView getMainText() {
        return mainText;
    }
}
