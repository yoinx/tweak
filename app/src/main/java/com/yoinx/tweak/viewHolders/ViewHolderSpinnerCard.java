package com.yoinx.tweak.viewHolders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.yoinx.tweak.R;
import com.yoinx.tweak.Tweak;
import com.yoinx.tweak.objects.SpinnerObject;

public class ViewHolderSpinnerCard extends RecyclerView.ViewHolder implements Spinner.OnItemSelectedListener,  View.OnTouchListener {
    private TextView mainText;
    private TextView supplementalText;
    private Spinner mSpinner;
    private Tweak.RecyclerAdapterListener mListener;
    private boolean userSelect = false;
    private SpinnerObject spinnerObject;

    public ViewHolderSpinnerCard(View v, Tweak.RecyclerAdapterListener listener) {
        super(v);
        mainText = v.findViewById(R.id.card_spinner_text_main);
        supplementalText = v.findViewById(R.id.card_spinner_text_supplemental);
        mSpinner = v.findViewById(R.id.card_spinner_choice_spinner_widget);
        mListener = listener;
        mSpinner.setOnTouchListener(this);
    }

    public void setSpinnerObject (SpinnerObject spin, Context context) {
        this.spinnerObject = spin;
        mainText.setText(spin.getCardText());
        supplementalText.setText(spin.getSupplementalText());
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, spinnerObject.getOptions());
        mSpinner.setAdapter(spinnerAdapter);
        mSpinner.setSelection(spinnerAdapter.getPosition(spinnerObject.getCurrentValue()));
        mSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        userSelect = true;
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (userSelect) {
            mListener.spinnerItemSelected(this.spinnerObject, position, mSpinner.getSelectedItem().toString());
            userSelect = false;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}