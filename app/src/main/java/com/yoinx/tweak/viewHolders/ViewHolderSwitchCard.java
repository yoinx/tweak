package com.yoinx.tweak.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.yoinx.tweak.R;
import com.yoinx.tweak.Tweak;
import com.yoinx.tweak.objects.SwitchObject;

public class ViewHolderSwitchCard extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
    private SwitchObject switchObject;
    private TextView mainText, supplementalText;
    private Switch mSwitch;
    private Tweak.RecyclerAdapterListener mListener;

    public ViewHolderSwitchCard(View v, Tweak.RecyclerAdapterListener listener) {
        super(v);
        mainText = v.findViewById(R.id.card_switch_text_main);
        supplementalText = v.findViewById(R.id.card_switch_text_supplemental);
        mSwitch = v.findViewById(R.id.card_switch_switch_widget);
        mListener = listener;
    }

    @Override
    public void onCheckedChanged(CompoundButton button, boolean state)
    {
        mListener.switchToggled(this.switchObject, state);
    }

    public void setSwitchObject(SwitchObject sw) {
        this.switchObject = sw;
        mainText.setText(sw.getCardText());
        supplementalText.setText(sw.getSupplementalText());
        mSwitch.setChecked(sw.getCurrentState());
    }

}
