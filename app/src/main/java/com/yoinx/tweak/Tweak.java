package com.yoinx.tweak;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.yoinx.tweak.utils.TweakValue;
import com.yoinx.tweak.fragments.SettingsFragment;
import com.yoinx.tweak.fragments.Tweakables;
import com.yoinx.tweak.objects.SeekbarObject;
import com.yoinx.tweak.objects.SpinnerObject;
import com.yoinx.tweak.objects.SwitchObject;
import com.yoinx.tweak.utils.Notification;

public class Tweak extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TweakValue tweakValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweak);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        tweakValue = new TweakValue(getApplicationContext());

        if (savedInstanceState == null) {
            //temporary, eventually this will be a dashboard as the first fragment.
            // Instance of first fragment
            Bundle bundle = new Bundle();
            Tweakables firstFragment = new Tweakables();
            bundle.putString("category", "cpu");
            firstFragment.setArguments(bundle);

            // Add Fragment to FrameLayout (flContainer), using FragmentManager
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
            ft.add(R.id.main_fragment, firstFragment);                                // add    Fragment
            ft.commit();

        }

        Notification notification = new Notification(getApplicationContext());
        notification.createNotificationChannel();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment frag = null;

        Bundle bundle = new Bundle();
        //bundle.putString("category", "");

        if (id == R.id.navigation_dashboard) {
            Log.d(getString(R.string.app_name), "Selected Menu Item: Dashboard");
        } else if (id == R.id.navigation_cpu) {
            frag = new Tweakables();
            bundle.putString("category", "cpu");
            frag.setArguments(bundle);
        } else if (id == R.id.navigation_voltages) {
            frag = new Tweakables();
            bundle.putString("category", "voltages");
            frag.setArguments(bundle);
        } else if (id == R.id.navigation_hotplugs) {
            frag = new Tweakables();
            bundle.putString("category", "hotplugs");
            frag.setArguments(bundle);
        } else if (id == R.id.navigation_graphics) {
            frag = new Tweakables();
            bundle.putString("category", "graphics");
            frag.setArguments(bundle);
        } else if (id == R.id.navigation_io) {
            frag = new Tweakables();
            bundle.putString("category", "io");
            frag.setArguments(bundle);
        } else if (id == R.id.navigation_power) {
            frag = new Tweakables();
            bundle.putString("category", "power");
            frag.setArguments(bundle);
        } else if (id == R.id.navigation_misc) {
            frag = new Tweakables();
            bundle.putString("category", "misc");
            frag.setArguments(bundle);
        } else if (id == R.id.navigation_settings) {
            frag = new SettingsFragment();
            Log.d(getString(R.string.app_name), "Selected Menu Item: Settings");
        } else if (id == R.id.navigation_about) {
            Log.d(getString(R.string.app_name), "Selected Menu Item: About");
        }

        if (frag != null) {
            FragmentManager fragMan = getSupportFragmentManager();
            fragMan.beginTransaction().replace(R.id.main_fragment, frag).commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public interface RecyclerAdapterListener {
        void switchToggled(SwitchObject sw, boolean state);
        void seekbarChanged(SeekbarObject seek, int position, int value);
        void spinnerItemSelected(SpinnerObject spin, int position, String value);
    }

}
