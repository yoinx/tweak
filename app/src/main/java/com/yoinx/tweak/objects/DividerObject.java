package com.yoinx.tweak.objects;

public class DividerObject extends Tweaks {
    private String mCardText;

    public DividerObject() {
        //empty constructor
    }

    public DividerObject(String cardText) {
        mCardText = cardText;
    }

    @Override
    public String getType() {
        return "Divider";
    }

    public DividerObject setCardText(String cardText) {
        this.mCardText = cardText;
        return this;
    }

    public String getCardText() {
        return this.mCardText;
    }

}
