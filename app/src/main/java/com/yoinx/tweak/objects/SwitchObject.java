package com.yoinx.tweak.objects;

import android.content.Context;

import com.yoinx.tweak.utils.SU;
import com.yoinx.tweak.utils.Settings;

public class SwitchObject extends Tweaks {
    private String mPath;
    private String mCardText;
    private String mSupplementalText;
    private SU su = new SU();
    private Settings settings;

    public SwitchObject() {
        //empty constructor
    }

    public SwitchObject(String path, String cardText, String supplementalText, Context context) {
        mPath = path;
        mCardText = cardText;
        mSupplementalText = supplementalText;
        settings = new Settings(context);
    }

    @Override
    public String getType() {
        return "Switch";
    }

    public SwitchObject setPath(String path) {
        this.mPath = path;
        return this;
    }

    public String getPath() {
        return this.mPath;
    }

    public SwitchObject setCardText(String cardText) {
        this.mCardText = cardText;
        return this;
    }

    public String getCardText() {
        return this.mCardText;
    }

    public SwitchObject setSupplementalText(String supplementalText) {
        this.mSupplementalText = supplementalText;
        return this;
    }

    public String getSupplementalText() {
        return this.mSupplementalText;
    }

    public boolean getCurrentState() {
        if (this.getPath().contains("/")) {
            String string = su.readFile(this.getPath());
            return (string.equals("1") || string.equalsIgnoreCase("t"));
        } else {
            return settings.getBool(this.getPath(), settings.getDefaultBool(this.getPath()));
        }
    }

    // this only exists to create a settings object
    public SwitchObject setContext (Context context) {
        settings = new Settings(context);
        return this;
    }
}
