package com.yoinx.tweak.objects;

import com.yoinx.tweak.utils.SU;

import java.util.ArrayList;

public class SpinnerObject extends Tweaks {
    private String mPath;
    private String mCardText;
    private String mSupplementalText;
    private String mCurrentValue;
    private ArrayList<String> mOptions;
    private SU su = new SU();

    public SpinnerObject() {
        //empty constructor
    }

    public SpinnerObject(String path, String cardText, String supplementalText, String currentValue, ArrayList<String> options) {
        mPath = path;
        mCardText = cardText;
        mSupplementalText = supplementalText;
        mCurrentValue = currentValue;
        mOptions = options;
    }

    @Override
    public String getType() {
        return "Spinner";
    }

    public SpinnerObject setPath(String path) {
        this.mPath = path;
        return this;
    }

    public String getPath() {
        return this.mPath;
    }

    public SpinnerObject setCardText(String cardText) {
        this.mCardText = cardText;
        return this;
    }

    public String getCardText() {
        return this.mCardText;
    }

    public SpinnerObject setSupplementalText(String supplementalText) {
        this.mSupplementalText = supplementalText;
        return this;
    }

    public String getSupplementalText() {
        return this.mSupplementalText;
    }

    public SpinnerObject setCurrentValue(String value) {
        mCurrentValue = value;
        return this;
    }

    public String getCurrentValue() {
        if (this.mCurrentValue == null) {
            // This will likely need more modifications
            String value = su.readFile(this.getPath());
            // Check if the value contains "[". This is for values like IO scheduler which has
            // "value1 [value2] value3" where the value in brackets is what is set
            if (value.contains("[")) {
                return value.substring(value.indexOf('[') + 1, value.indexOf(']'));
            } else {
                return value;
            }
        } else {
            return this.mCurrentValue;
        }
    }

    public SpinnerObject setOptions(ArrayList<String> options) {
        mOptions = options;
        return this;
    }

    public ArrayList<String> getOptions() {
        return mOptions;
    }

    public SpinnerObject addOption(String option) {
        mOptions.add(option);
        return this;
    }

    public void delOption(String option) {
        for (int i = 0; i < mOptions.size(); i++){
            if (mOptions.get(i).equals(option)) {
                mOptions.remove(i);
            }
        }
    }

}
