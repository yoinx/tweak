package com.yoinx.tweak.objects;

import com.yoinx.tweak.utils.SU;

public class SeekbarObject extends Tweaks {
    private String mPath;
    private String mCardText;
    private String mDescriptionText;
    private int mMin = 0;
    private int mMax = 100;
    private int mStep = 1;
    private int mValue = 50;
    private SU su = new SU();


    public SeekbarObject() {
        //empty constructor
    }

    public SeekbarObject(String path, String cardText, String descriptionText, int value, int min, int max, int step) {
        mPath = path;
        mCardText = cardText;
        mDescriptionText = descriptionText;
        mValue = value;
        mMin = min;
        mMax = max;
        mStep = step;
    }

    @Override
    public String getType() {
        return "Seekbar";
    }

    public SeekbarObject setPath(String path) {
        this.mPath = path;
        return this;
    }

    public String getPath() {
        return this.mPath;
    }

    public SeekbarObject setCardText(String cardText) {
        this.mCardText = cardText;
        return this;
    }

    public String getCardText() {
        return this.mCardText;
    }

    public SeekbarObject setDescriptionText(String descriptionText) {
        this.mDescriptionText = descriptionText;
        return this;
    }

    public String getDescriptionText() {
        return this.mDescriptionText;
    }

    public SeekbarObject setValue(int value) {
        this.mValue = value;
        return this;
    }

    public int getValue() {
        return Integer.valueOf(su.readFile(this.mPath).trim());
    }

    public SeekbarObject setMin(int min) {
        this.mMin = min;
        return this;
    }

    public int getMin() {
        return this.mMin;
    }

    public SeekbarObject setMax(int max) {
        this.mMax = max;
        return this;
    }

    public int getMax() {
        return this.mMax;
    }

    public SeekbarObject setStep(int step) {
        this.mStep = step;
        return this;
    }

    public int getStep() {
        return this.mStep;
    }
}
