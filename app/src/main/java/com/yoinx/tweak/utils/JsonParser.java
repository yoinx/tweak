package com.yoinx.tweak.utils;

import android.content.Context;
import android.widget.Toast;

import com.yoinx.tweak.objects.DividerObject;
import com.yoinx.tweak.objects.SeekbarObject;
import com.yoinx.tweak.objects.SpinnerObject;
import com.yoinx.tweak.objects.SwitchObject;
import com.yoinx.tweak.objects.Tweaks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class JsonParser {

    private Context mContext;
    private SU su = new SU();

    public JsonParser(Context context) {
        mContext = context;
    }

    public ArrayList<Tweaks> getTweaks(String category) {
        ArrayList<Tweaks> tweaks = new ArrayList<>();
        // Object Names
        final String DIVIDER = "Divider";
        final String SWITCH = "Switch";
        final String SEEKBAR = "SeekBar";
        final String SPINNER = "Spinner";

        // Field Names
        final String TYPE = "type";
        final String MAINTEXT ="text";
        final String SUPTEXT = "supplemental_text";
        final String PATH = "path";
        final String MAX = "max";
        final String MIN = "min";
        final String STEP = "step";
        final String OPTIONS = "options";

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(category));
            JSONArray items = obj.getJSONArray(category);
            DividerObject mDivider;
            SwitchObject mSwitch;
            SeekbarObject mSeekBar;
            SpinnerObject mSpinner;

            for (int i=0;i<items.length();i++){
                JSONObject jsonObject=items.getJSONObject(i);

                if (jsonObject.getString(TYPE).equalsIgnoreCase(DIVIDER)) {
                    mDivider = new DividerObject()
                            .setCardText(jsonObject.getString(MAINTEXT));
                    tweaks.add(mDivider);
                } else if (jsonObject.getString(TYPE).equalsIgnoreCase(SWITCH)) {
                    mSwitch = new SwitchObject()
                            .setCardText(jsonObject.getString(MAINTEXT))
                            .setSupplementalText(jsonObject.getString(SUPTEXT))
                            .setPath(jsonObject.getString(PATH));
                    tweaks.add(mSwitch);
                } else if (jsonObject.getString(TYPE).equals(SEEKBAR)) {
                    mSeekBar = new SeekbarObject()
                            .setCardText(jsonObject.getString(MAINTEXT))
                            .setDescriptionText(jsonObject.getString(SUPTEXT))
                            .setPath(jsonObject.getString(PATH))
                            .setMax(Integer.valueOf(jsonObject.getString(MAX)))
                            .setMin(Integer.valueOf(jsonObject.getString(MIN)))
                            .setStep(Integer.valueOf(jsonObject.getString(STEP)));
                    tweaks.add(mSeekBar);
                } else if (jsonObject.getString(TYPE).equalsIgnoreCase(SPINNER)) {
                    String optString = jsonObject.getString(OPTIONS);
                    // Assume that values are being read from a path.
                    if (optString.contains("/")) {
                        optString = su.readFile(optString).replaceAll("[\\[\\]]*", "");
                    }
                    ArrayList<String> options = new ArrayList<>(Arrays.asList(optString.split(" ")));

                    mSpinner = new SpinnerObject()
                            .setCardText(jsonObject.getString(MAINTEXT))
                            .setSupplementalText(jsonObject.getString(SUPTEXT))
                            .setPath(jsonObject.getString(PATH))
                            .setOptions(options);
                    tweaks.add(mSpinner);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(mContext,"JSON ERROR.", Toast.LENGTH_SHORT).show();
        }
        return tweaks;
    }

    private String loadJSONFromAsset(String file) {
        String json;
        try {
            InputStream is = mContext.getAssets().open(file + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return "";
        }
        return json;
    }
}
