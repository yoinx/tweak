package com.yoinx.tweak.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

public class Settings {

    private SharedPreferences settings;
    public final String VERBOSELOGGING = "VerboseLogging";
    public final String APPLYONBOOTDELAY = "ApplyOnBootDelay";

    public final String SETTINGS = "settings";
    public final String STABILITY = "Stability";
    public final String KERNELVALUES = "KernelValues";

    public final String STABILITY_KERNEL = "Kernel";

    public Settings (Context context) {
        settings = context.getApplicationContext().getSharedPreferences(SETTINGS, 0);
    }

    public Settings (Context context, String settingsFile) {
        if (settingsFile.equals(SETTINGS)) {
            settings = context.getApplicationContext().getSharedPreferences(SETTINGS, 0);
        } else if (settingsFile.equals(STABILITY)) {
            settings = context.getApplicationContext().getSharedPreferences(STABILITY, 0);
        } else if (settingsFile.equals(KERNELVALUES)) {
            settings = context.getApplicationContext().getSharedPreferences(KERNELVALUES, 0);
        }
    }

    public void saveSetting (String key, String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void saveSetting (String key, boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void saveSetting (String key, int value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public String getString(String key, String mDefaultValue) {
        return settings.getString(key, mDefaultValue);
    }

    public boolean getBool(String key, boolean mDefaultValue) {
        return settings.getBoolean(key, mDefaultValue);
    }

    public int getInt(String key, int mDefaultValue) {
        return settings.getInt(key, mDefaultValue);
    }

    public boolean getDefaultBool (String key) {
        switch (key) {
            case VERBOSELOGGING: return false;
            default: return false;
        }
    }

    public int getDefaultInt(String key) {
        switch (key) {
            case APPLYONBOOTDELAY: return 5;
            default: return 1;
        }
    }

    public HashMap<String, String> getAll() {
        Map<String, ?> allEntries = settings.getAll();
        HashMap<String, String> savedSettings = new HashMap<>();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            savedSettings.put(entry.getKey(), entry.getValue().toString());
        }
        return savedSettings;
    }
}
