package com.yoinx.tweak.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.yoinx.tweak.R;

import java.util.HashMap;
import java.util.Map;

public class TweakValue {

    private Settings kernelValues;
    private Settings settings;
    private Settings stabilityCheck;
    private SU su = new SU();
    private Context mContext;
    private static String kernelVersion = null;

    public TweakValue(Context context) {
        mContext = context;
        settings = new Settings(mContext);
        kernelValues = new Settings(mContext, settings.KERNELVALUES);
        stabilityCheck = new Settings(mContext, settings.STABILITY);
        kernelVersion = su.getSyncCommandOutput("uname -r");
    }

    /***
     *  Saves the passed value to the KernelValues SharedPreferences, then passes it to the SU class to write out to sysfs.
     */

    public void saveValue(String path, String value) {
        if (settings.getBool(settings.VERBOSELOGGING, false)) {
            Log.v(mContext.getString(R.string.app_name), "Writing Value: '" + value + "' to path: '" + path +"'");
        }
        kernelValues.saveSetting(path, value);
        if (kernelVersion == null) {
            kernelVersion = su.getSyncCommandOutput("uname -r");
        }
        stabilityCheck.saveSetting(stabilityCheck.STABILITY_KERNEL, kernelVersion);

        su.writeFile(path, value);
    }

    /***
     *  Helper functions to allow overloading saveValue with boolean or int values.
     */

    public void saveValue(String path, boolean value) {
        if (value) {
            saveValue(path, "1");
        } else {
            saveValue(path, "0");
        }
    }


    public void saveValue(String path, int value) {
        saveValue(path, String.valueOf(value));
    }

    public String getValue (String path) {
        return kernelValues.getString(path, null);
    }

    /***
     * This function is simply to dump all saved values.
     *
     * @return Hashmap of all saved values in path=value pairs.
     */

    public HashMap<String, String> getAll() {
        return kernelValues.getAll();
    }

}
