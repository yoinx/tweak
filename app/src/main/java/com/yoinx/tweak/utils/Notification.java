package com.yoinx.tweak.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.yoinx.tweak.R;

import java.util.Timer;
import java.util.TimerTask;

public class Notification {
    private Context mContext;
    private String CHANNEL_ID = "TweakNotificationChannel";

    public Notification(Context context) {
        mContext = context;
    }

    public void sendNotification(String title, @Nullable String text, final int notificationId, final int timeDelay) {

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(title)
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setOnlyAlertOnce(true);

        final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mContext);

        if (timeDelay > 0) {
            mBuilder.setProgress(timeDelay, 0, false);
            new Timer().scheduleAtFixedRate(new TimerTask() {
                int x = timeDelay - 1;
                int currentProgress = 0;
                @Override
                public void run() {
                    currentProgress++;
                    if (x == 0) {
                        this.cancel();
                    } else {
                        x--;
                        mBuilder.setProgress(timeDelay, currentProgress, false);
                        notificationManager.notify(notificationId, mBuilder.build());
                    }
                }
            }, 1000, 1000);
        }
        notificationManager.notify(notificationId, mBuilder.build());

    }

    public void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = mContext.getString(R.string.app_name);
            String description = mContext.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = mContext.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
