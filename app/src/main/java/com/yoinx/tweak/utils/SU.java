package com.yoinx.tweak.utils;

import com.topjohnwu.superuser.BusyBox;
import com.topjohnwu.superuser.Shell;
import com.topjohnwu.superuser.io.SuFile;
import com.topjohnwu.superuser.io.SuFileOutputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.List;

public class SU extends Shell.ContainerApp {

    @Override
    public void onCreate() {
        super.onCreate();
        // Set libsu flags and use internal busybox
        Shell.setFlags(Shell.FLAG_REDIRECT_STDERR);
        BusyBox.setup(this);
    }

    public void runCommand (String command) {
        Shell.Async.su(command);
    }

    public String getSyncCommandOutput (String command) {
        StringBuilder sb = new StringBuilder("");
        List<String> result = Shell.Sync.su(command);
        for (int i = 0; i < result.size(); i++) {
            sb.append(result.get(i)).append("\n");
        }
        return sb.toString().trim();
    }

    /***
     *
     * @param path filepath to read from
     * @return string containing file contents, will return "" if nothing else.
     */
    public String readFile (String path) {
        File mFile = new SuFile(path);

        StringBuilder sb = new StringBuilder("");
        if (mFile.exists()) {
            try (FileInputStream is = new FileInputStream(mFile)) {
                int c;
                while ((c = is.read()) != -1) {
                    sb.append((char) c);
                }
            } catch (FileNotFoundException e) {
                // Hacky way of reading files that need root, file.canRead returns true for unreadable files. Which results in a Permission error.
                // In the future, I may just perform the 'cat' on everything rather than waiting for the catch if this causes a performance impact.
                sb.append(getSyncCommandOutput("cat " + path));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString().trim();
    }

    /***
     *
     * @param path filepath to write to
     * @param value value to write
     * @return returns true if the write did not throw an exception
     */
    public boolean writeFile (String path, String value) {
        SuFile mFile = new SuFile(path);
        if (!mFile.exists()) {
            try {
                mFile.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try (OutputStream os = new SuFileOutputStream(mFile)) {
            byte[] bValue = value.getBytes();
            os.write(bValue);
            os.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}